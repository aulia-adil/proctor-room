# Proctor Room

Proctor Room adalah infrastruktur untuk melakukan pengawasan kepada peserta ujian yang menggunakan Moodle. Harapan dari infrastruktur ini adalah dapat dilakukan pencegahan kecurangan ujian.

## Penjelasan Lebih Detail

1. [Video sidang](https://youtu.be/MFZmVBooDp4)
2. [Makalah skripsi](https://www.researchgate.net/publication/362057514_REAL_TIME_STREAM_PROCESSING_AKTIVITAS_UJIAN_PILIHAN_GANDA_PADA_FITUR_KUIS_DI_PLATFORM_MOODLE)

## Environment yang penulis gunakan

Penulis menjalankan Proctor Room dengan menggunakan [AWS EC 2 t2.large instance](https://aws.amazon.com/id/ec2/instance-types/). Berikut adalah spesifikasi dari environment tersebut:

- Ubuntu 18.04
- Storage 20 GB
- RAM 8 GB
- CPU 2 core

## Instalasi Proctor Room

Copy-paste hal-hal ini pada server tersebut

``` 
git clone https://gitlab.com/aulia-adil/proctor-room.git
cd proctor-room
sh run.sh

# Untuk menyalakan websocket
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 & 
```

## Open Grafana untuk cek keberhasilan

1. Buka Grafana: http://public-ip-address:3000
Pada saat penulisan ini, Grafana memiliki username dan password default: admin
1. Add data source InfluxDB dengan URL private-ip-address:8086 , database name: `t_integration_learninganalytics` , HTTP Method GET. Perlu diingat private-ip-address di sini bukanlah localhost.
2. Dengan menggunakan Grafana, lakukan query kepada influxDB: `SELECT * FROM "t-scele-asynchronous"`

## Jika membuka VM yang sudah dimatikan

Lakukan: 
```
sh rerun.sh

# Untuk menyalakan websocket
sudo docker exec -it moodle nohup php /var/www/html/moodle/mod/quiz/accessrule/wifiresilience/websocket.php > ~/websocket-output.log 2>&1 & 
```

## Membuat Load Testing

1. Masuk ke docker Moodle dengan command: `sudo docker exec -ti f3859 bash`
2. Pergi ke `config.php` dari Moodle dengan command: `cd /var/www/html/moodle/`. Di sana ada `config.php`
3. Install vim atau yang semacamnya: `apt update && apt install vim`
4. Konfigurasi `config.php` dengan `vim config.php`
5. Cari `examplepassword` dengan `/` di VIM
6. Uncomment line of code tersebut
7. Restart Apache2 dengan `service Apache2 restart`
8. Pergi ke Moodle dan aktifkan Developer mode
9. Buat kelas yang berisi `n` murid
10. Pergi ke Jmeter test plan
11. Buat test plan dengan `n` murid tersebut
12. Checklist update password
13. Submit dan download data muridnya
