FROM registry.gitlab.com/aulia-adil/proctor-room/moodle:latest

ARG dbhost
ARG publicIpAddress
ARG CFG='$CFG'

RUN sed -i 's+/var/www/html+/var/www/html/moodle+g' /etc/apache2/sites-available/000-default.conf && \
    sed -i "s+'localhost';+'$dbhost';+g" /var/www/html/moodle/config.php && \
    sed -i "s+'moodle';+'postgres';+g" /var/www/html/moodle/config.php && \
    sed -i "s+'username';+'moodleuser';+g" /var/www/html/moodle/config.php && \
    sed -i "s+'password';+'moodleuser';+g" /var/www/html/moodle/config.php && \
    sed -i "s+example.com/moodle';+$publicIpAddress';\n\$CFG->wsroot = 'ws://$publicIpAddress';\n\$CFG->wsport = 5000;+g" /var/www/html/moodle/config.php && \
    sed -i "s+/home/example/moodledata+/var/moodledata+g" /var/www/html/moodle/config.php
